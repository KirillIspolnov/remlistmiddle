package org.kbff.edu.remlistmiddle;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {
	static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		ArrayList<Integer> integers = new ArrayList<Integer>();
		
		int count = scanner.nextInt();
		for(int i = 0; i < count; i++) {
			integers.add(scanner.nextInt());
		}
		
		System.out.println(integers);
		try {
			removeMiddle(integers);
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println(integers);
	}
	
	public static int removeMiddle(List<Integer> origin) throws Exception {
		int size = origin.size();
		if(size % 2 == 0) throw new Exception("Список содержит чётное количество чисел. Удалить элемент из середины не удастся");
		return origin.remove(size / 2);
	}

}
