package org.kbff.edu.remlistmiddle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class SolutionTest {

	@Test
	public void testRemoveMiddle() {
		try(FileReader freader = new FileReader("tests.json");
			BufferedReader breader = new BufferedReader(freader)) {
			JsonParser jparser = new JsonParser();
			JsonArray root = jparser.parse(breader).getAsJsonArray();
			for(int i = 0; i < root.size(); i++) {
				JsonObject suite = root.get(i).getAsJsonObject();
				List<Integer> array = convert(suite.get("array").getAsJsonArray());
				
				try {
					int middle = Solution.removeMiddle(array);
					assertEquals(suite.get("middle").getAsInt(), middle);
				} catch(Exception e) {
					boolean err = suite.get("has-error").getAsBoolean();
					assertTrue(err);
				}
			}
		} catch(Throwable t) {
			t.printStackTrace();
		}
	}

	public List<Integer> convert(JsonArray array) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		for(int i = 0; i < array.size(); i++) {
			result.add(array.get(i).getAsInt());
		}
		return result;
	}
}
